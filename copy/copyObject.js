function deepCopy(obj, clonedObjects = new WeakMap()){
    if (clonedObjects.has(obj)) {
        return clonedObjects.get(obj);
    }

    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    if (obj instanceof Date) {
        return new Date(obj);
    }
    if (obj instanceof Map) {
        return new Map(obj);
    }
    if (obj instanceof Set) {
        return new Set(obj);
    }

    const newObj= Array.isArray(obj) ? [] : {};

    clonedObjects.set(obj, newObj);

    for (let key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            newObj[key] = deepCopy(obj[key], clonedObjects);
        }
    }

    return newObj;
}

const original = {
    name: {
        firstName: "John",
        lastName: "Doe"
    },
    age: 30,
    hobbies: ["reading", "hiking"],
    address: {
        city: "New York",
        country: "USA"
    }
};

const copied = deepCopy(original);

copied.name.firstName = "Jane";
copied.age = 25;
copied.hobbies.push("swimming");
copied.address.city = "Los Angeles";

console.log("Объект оригинал:", original);
console.log("Объект который скопировали и изменили:", copied);